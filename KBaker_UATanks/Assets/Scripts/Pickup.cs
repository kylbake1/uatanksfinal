﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Powerup powerup; // creates a holder for our Powerup and names it powerup
    public AudioClip feedback; // renames the audioclip feedback 
    public GameObject pickupPrefab; //creates a holder in the inspector for our pickup prefab
    public GameObject spawnedPickup; // creates a holder in the inspector for our spawned pickups
    public float spawnDelay; // creates a float that will determine the delay of the pickup for the designers

    private float nextSpawnTime; //  creates a private float that the designers cant modify
    private Transform tf; // renames the transform componet tf


    // Start is called before the first frame update
    void Start()
    {
        tf = gameObject.GetComponent<Transform>(); // get the tf and automatically attached the Transform componet in the start of the game
        nextSpawnTime = Time.time + spawnDelay; // this will begin the spawn delay
    }

    // Update is called once per frame
    void Update()
    {       
        if (spawnedPickup == null) // If it is there is nothing spawns
        {
           if (Time.time > nextSpawnTime) // And it is time to spawn
            {
                spawnedPickup = Instantiate(pickupPrefab, tf.position, Quaternion.identity) as GameObject; // this will spawn the pickup and make it a GameObject 
                nextSpawnTime = Time.time + spawnDelay; // This will reset the time of the pickup
            }
        }
        else
        {
            nextSpawnTime = Time.time + spawnDelay; // If the powerup isnt picked by the time the timer has ended, it will just place another item ontop so it wont be destroyed until pickup
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        PowerupController powCon = other.GetComponent<PowerupController>(); // this will rename the powerupContoller powcon and will automatically set the powerup contoller script to it
        if(powCon != null)
        {
            powCon.Add(powerup); // if the power up is picked up, activate power
            if(feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, tf.position, 1.0f); // this will play the sound when a item is picked up
            }
            Destroy(gameObject); // the powerup is destroyed
        }
    }

}
