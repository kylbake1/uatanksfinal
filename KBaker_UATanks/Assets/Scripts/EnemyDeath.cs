﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet") // by a bullet with tag Bullet
        {
            Destroy(this.gameObject);// and is destroyed             
            Destroy(collision.gameObject);// Also destroy bullet
        }
    }
}
