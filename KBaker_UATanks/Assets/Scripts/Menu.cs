﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public AudioMixer audioMixer; // This will use the audioMixer
    public Dropdown resolutionDropdown; // this will use the dropdown UI
    public GameManager manage; // this will hold our gameManager

    public Slider[] volumeSliders;

    Resolution[] resolutions; // This array will hold all of the computer resolutions

    void Start()
    {
        resolutions = Screen.resolutions; // This will set the current resoluton for us automatically

        resolutionDropdown.ClearOptions(); // This will clear out all the resolutons in the resolutions dropdown

        List<string> options = new List<string>(); // create a new list of strings that will be our new options

        int currentResolutionIndex = 0;

        for (int i = 0; i < resolutions.Length; i++) // Then loop through reach resolutions in the array for us to see
        {
            string option = resolutions[i].width + " X " + resolutions[i].height; // We give each of the resolutions a nice name
            options.Add(option); // Than we add the new name to our options list

            if(resolutions[i].width == Screen.currentResolution.width && // This compare both the width
                resolutions[i].height == Screen.currentResolution.height) // and the height of the resolutions
            {
                currentResolutionIndex = i; // If both resolutons match up, that is our current resolution
            }
        }

        resolutionDropdown.AddOptions(options); // Lastly the resoution will go into the drop down
        resolutionDropdown.value = currentResolutionIndex; // 
        resolutionDropdown.RefreshShownValue();
        manage = GetComponent<GameManager>();
    }

   

    public void PlayGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");
        Application.Quit();
    }


    public void LoadMenu()
    {
        Time.timeScale = 1f;
        Debug.Log("Loading Menu...");
        SceneManager.LoadScene(0);
    }

    public void SetVolume (float volume)
    {
        audioMixer.SetFloat("Volume", volume);
    }

    public void SetQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetFullScreen(bool isFullScreen)
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex]; // creates a resolution variable and this will find our resolution we want automatically
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void PlayerTwoReady(bool isTwoPlayer)
    {
        GameManager.instance.isTwoPlayer = isTwoPlayer;
    }

    public void SetMusicVolume(float value)
    {
        
    }

    public void SetSfxVolume(float value)
    {

    }
}
