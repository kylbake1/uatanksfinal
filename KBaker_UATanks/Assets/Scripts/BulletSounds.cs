﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSounds : MonoBehaviour
{
    public AudioSource blast;

    // Start is called before the first frame update
    void Awake()
    {
        blast = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnCollisionEnter(Collision wall) // This is OnCollisionEnter that is called bullet for ease
    {
        if (wall.gameObject.tag == "Bullet") // if the bullet makes contact with the wall tagged "Wall"
        {
            blast.Play();
        }
    }
}
