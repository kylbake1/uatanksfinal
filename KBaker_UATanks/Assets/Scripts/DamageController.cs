﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageController : MonoBehaviour
{
    [SerializeField] private float cubeDamage;
    [SerializeField] private HealthController healthController;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Damage();
        }
        if (other.CompareTag("PlayerTwo"))
        {
            DamageTwo();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Damage()
    {
        healthController.playerHealth = healthController.playerHealth - cubeDamage;
        healthController.UpdateHealth();
        this.gameObject.SetActive(false);
    }
    public void DamageTwo()
    {
        healthController.playerHealthTwo = healthController.playerHealthTwo - cubeDamage;
        healthController.UpdateHealthTwo();
        this.gameObject.SetActive(false);
    }
}
