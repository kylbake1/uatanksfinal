﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseMaker : MonoBehaviour
{
    // This section is public and customizable in the inspector for the designers
    public TankData data; // obtains the TankData script and names it data

    // This section is private and is not customizable to the designers
    private TankMotor motor; // obtains the TankMotor script and names it motor

    private void Update()
    {
        if(data.volume > 0) // if statement that uses the volume from the data script and if it's greater than 0
        {
            data.volume -= data.decayPerFrameDraw; // the volume will decrease over time from set time by designers
        }
    }
}
