﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickItem : MonoBehaviour
{
    public static GameManager instance;

    // Start is called before the first frame update
    void Start()
    {
        Pick(); // this will run the pick fuction
    }

    public void Pick()
    {
        int randomIndex = Random.Range(0,GameManager.instance.AisToPickFrom.Length); // this will select a number from 0 to the amount of AI's in the array to choose from to spawn into the game
        GameObject clone = Instantiate(GameManager.instance.AisToPickFrom[randomIndex], transform.position, Quaternion.identity); // this will create a clone of the prefab and spawn in a AI from the index, put it in a location, and keep the current rotations 
    }
}
