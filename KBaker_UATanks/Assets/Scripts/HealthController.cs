﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthController : MonoBehaviour
{
    public float playerHealth; // This will control the player one health
    public float playerHealthTwo; // This will control the player two health
    [SerializeField] private Text healthText; // This will allow us to reach the text from any script
    [SerializeField] private Text healthTextTwo; // This will also allow us to reach any script for player 2

public void Update()
    {
        if (playerHealthTwo <= 0 && playerHealth <= 0) // if player one and two lives are 0 then the game will end
        {
            Gameover(); // game over function will start
        }
    }
    // Update is called once per frame
    public void UpdateHealth() // this will update our health when player takes damage
    {
        healthText.text = playerHealth.ToString("0"); // this will update the number of health
    }

    public void UpdateHealthTwo() // this will do the same as the above script for player one but for player 2
    {
        healthTextTwo.text = playerHealthTwo.ToString("0");
    }
    
    public void Gameover() // this will initiate game over and close the game
    {
            Debug.Log("Game Over!");
            Application.Quit();// will quit the game when built
    }


}
