﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{
    public Camera camOne;

    // Start is called before the first frame update
    void Start()
    {
        camOne = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.isTwoPlayer)
        {
            TwoPlayerEnabled();
        }
        else
        {
            TwoPlayerDisabled();
        }
    }

    public void TwoPlayerEnabled()
    {
        camOne.rect = new Rect(0, 0, 1f, 0.5f);        
    }

    public void TwoPlayerDisabled()
    {
        camOne.rect = new Rect(0, 0, 1f, 1f);
    }
}
