﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    public static GameManager instance; // this will allow us to use the GameManager script

    // Start is called before the first frame update
    void Start()
    {
        SpawnPlayerOne(); // this will run the function when the program starts
        if (GameManager.instance.isTwoPlayer)
        {
            SpawnPlayerTwo();
        }
    }

    public void SpawnPlayerOne()
    {
        Vector3 randPosition = new Vector3(Random.Range(-GameManager.instance.itemXSpread, GameManager.instance.itemXSpread), Random.Range(-GameManager.instance.itemYSpread, GameManager.instance.itemYSpread), Random.Range(-GameManager.instance.itemZSpread, GameManager.instance.itemZSpread)) + transform.position; // this will give us a random location between the coordiates that the designer will enter
        GameObject clone = Instantiate(GameManager.instance.playerToSpread, randPosition, Quaternion.identity); // this will make a clone of our prefab, place the clone in a random spot, and keep it from rotating when placed.
    }

    public void SpawnPlayerTwo()
    {
        Vector3 randPositionTwo = new Vector3(Random.Range(-GameManager.instance.itemXSpread, GameManager.instance.itemXSpread), Random.Range(-GameManager.instance.itemYSpread, GameManager.instance.itemYSpread), Random.Range(-GameManager.instance.itemZSpread, GameManager.instance.itemZSpread)) + transform.position; // this will give us a random location between the coordiates that the designer will enter
        GameObject cloneTwo = Instantiate(GameManager.instance.playerToSpreadTwo, randPositionTwo, Quaternion.identity); // this will make a clone of our prefab, place the clone in a random spot, and keep it from rotating when placed.
    }
}